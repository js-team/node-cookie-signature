# Installation
> `npm install --save @types/cookie-signature`

# Summary
This package contains type definitions for cookie-signature (https://github.com/tj/node-cookie-signature).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/cookie-signature.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/cookie-signature/index.d.ts)
````ts
/// <reference types="node" />

import type { CipherKey } from "node:crypto";

/** Sign the given `val` with `secret`. */
export function sign(value: string, secret: CipherKey): string;

/**
 * Unsign and decode the given `val` with `secret`,
 * returning `false` if the signature is invalid.
 */
export function unsign(value: string, secret: CipherKey): string | false;

````

### Additional Details
 * Last updated: Mon, 06 Nov 2023 22:41:05 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)

# Credits
These definitions were written by [Junyoung Choi](https://github.com/Rokt33r).
